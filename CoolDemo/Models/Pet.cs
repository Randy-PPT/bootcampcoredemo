﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoolDemo.Models
{
    public class Pet
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "WE NEED THIS INFO"), MinLength(3, ErrorMessage = "NAME TOO SHORT"), MaxLength(20, ErrorMessage = "Name too long")]
        public string Name { get; set; }
        [Required, DisplayName("How Old")]
        public int? Age { get; set; }

        [ForeignKey("PetOwner")]
        public int? OwnerId { get; set; }

        public Owner PetOwner { get; set; }

        //public int? OwnerId { get; set; }
        //[ForeignKey("OwnerId")]
        //public Owner Owner { get; set; }
    }
}
